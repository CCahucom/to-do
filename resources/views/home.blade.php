<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>To-Do List</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>
</head>

<style>
    body {
        background-color: #41a8f1;
    }
</style>

<!--<body class="bg-info">    Default Background  --> 

    <nav class="navbar navbar-dark" style="background-color: #0a588f;"> <!-- Navigation Bar -->
        <div class="container">
            <div class="navbar-header">
                <!-- Branding Image -->
                <a class="navbar-brand" style="color: white" href="{{ url('/') }}">
                    <b>To-Do List</b>
                </a>
            </div>
        </div>
    </nav>

    <div class="container w-50 mt-5 ">
        <div class="shadow p-3 mb-5 bg-white rounded">
            <div class="card-body">
                <form action="{{ route('store') }}" method="POST" autocomplete="off">
                    @csrf
                    <div class="input-group" style="text-align: center">
                        <input type="text" name="content" class="form-control" placeholder="Task Name">
                        <button type="submit" class="btn btn-success"><i class="fa fa-btn fa-plus"></i> </button>
                    </div>  
                </form>
                {{-- if tasks count --}} 
                @if (count($todolists))  
                    <ul class="list-group list-group-flush mt-3">    
                        @foreach ($todolists as $todolist)
                            <li class="list-group-item">                              
                                <!-- Delete -->
                                <form action="{{ route('destroy', $todolist->id) }}" method="POST">
                                    {{ $todolist->content }}
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger" style="float: right"><i class="fa fa-trash"></i>  </button>
                                </form>
                                <!-- Edit  -->   
                                <a href="{{ route('edit', $todolist->id) }}" class='btn btn-primary' style="float: right; margin-right: 10px; margin-top: -25px"><i class="fa fa-pencil-square-o"></i>  </a>       
                            </li>
                        @endforeach     
                    </ul>
                @else
                <p class="text-center mt-3">No Tasks!</p>   
                @endif 
            </div>   
            @if (count($todolists))   
                <div class="card-footer">
                    You have {{  count($todolists) }} remaining tasks.
                </div>    
            @else 
            @endif
        </div>  
    </div>         
</body>
</html>


