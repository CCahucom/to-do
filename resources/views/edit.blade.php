<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>To-Do List</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>
</head>

<style>
    body {
        background-color: #41a8f1;
    }
</style>

<!--<body class="bg-info">    Default Background  --> 

    <nav class="navbar navbar-dark" style="background-color: #0a588f;"> <!-- Navigation Bar -->
        <div class="container">
            <div class="navbar-header">
                <!-- Branding Image -->
                <a class="navbar-brand" style="color: white" href="{{ url('/') }}">
                    <b>To-Do List</b>
                </a>
            </div>
        </div>
    </nav>
    <div class="container w-25 mt-5 "> 
        <div class="shadow p-3 mb-5 bg-white rounded">
            @foreach ($todolists as $todolist)
                <form action="{{ route('update', [$todolist->id]) }}" method='POST'>
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT"> 
                    <div class="input-group" style="text-align: center">
                        <input type="text" name="updatedlistname" class="form-control" value="{{ $todolist->content }} ">
                    </div>
                    <div class="form-group" style="margin-top: 10px; text-align: right;">
                        <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Save </button>
                        <a href="{{ url('/') }}" class='btn btn-danger'><i class="fa fa-arrow-left"></i> Back </a>
                    </div> 
                </form>
            @endforeach
        </div>  
    </div>         
</body>
</html>


