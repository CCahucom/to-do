<?php

namespace App\Http\Controllers;

use App\Models\Todolist;
use Illuminate\Http\Request;

class TodolistController extends Controller
{
    
    public function index()
    {
        $todolists = Todolist::all();
        return view('home', compact('todolists'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'content' => 'required'
        ]);

        Todolist::create($data);
        return back();
    }

    public function edit(Todolist $todolist)
    {
        $todolists = Todolist::find($todolist);
        return view('edit', compact('todolists'));
    }

    public function update(Request $request, $todolist)
    {
        $todolists = Todolist::find($todolist);
        $todolists->content = $request->updatedlistname;
        $todolists->save();

        return redirect()->route('index');
    }
    
    public function destroy(Todolist $todolist)
    {
        $todolist->delete();
        return back();
    }
}
